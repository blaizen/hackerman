﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    NavMeshAgent agent;

    public InputField hackingInput;

    GameObject dest;
    public GameObject destObj;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

	void Update ()
    {
        if (Input.GetMouseButton(0))
        {
            MovePlayer();
        }

        if(dest)
        {
            float dist = Vector3.Distance(transform.position, dest.transform.position);
            if(dist <= 0.2f)
            {
                Destroy(dest);
            }
        }
	}

    void MovePlayer()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Ground")))
        {

            if (hit.transform.gameObject.CompareTag("Ground") && !hackingInput.GetComponent<DisableRaycast>().inUI)
            {
                agent.destination = hit.point;
                if(dest)
                {
                    Destroy(dest);
                }
                dest = Instantiate(destObj, hit.point, transform.rotation);
            }
        }
    }
}
