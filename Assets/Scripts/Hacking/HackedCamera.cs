﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackedCamera : MonoBehaviour
{
    public int maxTime = 10;

    void Hacked(CommandData cData)
    {
        switch (cData.command)
        {
            case "disable":
                SurCamera cam = GetComponent<SurCamera>();
                StartCoroutine(cam.Disabled(cData.seconds));
                break;
            default:
                GlobalEvents.instance.WrongCommand();
                break;
        }
    }
}
