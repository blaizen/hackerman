﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class HackingHandler : MonoBehaviour {

    public InputField commandInputField;
    string[] commandLine;
    public List<string> commandLineList = new List<string>();

    private readonly char[] WORD_SEPARATOR = new char[] { '\n', ' ', '\r' };
	
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            TakeCommand();
        }		
	}

    void TakeCommand()
    {
        //Take the command the player has entered and place commands into a list.
        commandLine = commandInputField.text.Split(WORD_SEPARATOR);
        commandInputField.text = string.Empty;
        commandLineList = commandLine.ToList();
        //Remove empty strings from list.
        commandLineList.RemoveAll(x => string.IsNullOrEmpty(x));
        //Place commands into data package class.
        CommandData data = new CommandData();
        data.hackedObj = GameObject.Find(commandLineList[0]);
        data.command = commandLineList[1];
        data.seconds = int.Parse(commandLineList[2]);
        //If we found the object the player has entered, send all of the information to that object.
        if(data.hackedObj)
        {
            data.hackedObj.SendMessage("Hacked", data);
        }
        else
        {
            GlobalEvents.instance.WrongCommand();
        }
    }
}
