﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HackedDoor : MonoBehaviour
{
    //maxTime defines the maximum time a command can run for.

        //TODO: MAKE A STATE MACHINE!!!
    public int maxTime = 10;
    public bool doorStartOpen;

    void Hacked(CommandData cData)
    {
        if (cData.seconds > maxTime)
        {
            cData.seconds = maxTime;
        }
        switch (cData.command)
        {
            case "open":
                if (!doorStartOpen)
                    StartCoroutine(Open(cData.seconds));
                else
                    GlobalEvents.instance.WrongCommand();
                break;
            case "color=red":
                StartCoroutine(ColorRed(cData.seconds));
                break;
            case "close":
                if (doorStartOpen)
                    StartCoroutine(Close(cData.seconds));
                else
                    GlobalEvents.instance.WrongCommand();
                break;
            default:
                GlobalEvents.instance.WrongCommand();
                break;
        }
    }

    IEnumerator Open(int time)
    {
        Vector3 startPos = gameObject.transform.position;
        Vector3 openPos = new Vector3(startPos.x, startPos.y + 1, startPos.z);
        gameObject.transform.position = openPos;
        yield return new WaitForSeconds(time);
        gameObject.transform.position = startPos;
    }

    IEnumerator Close(int time)
    {
        Vector3 startPos = gameObject.transform.position;
        Vector3 openPos = new Vector3(startPos.x, startPos.y - 1, startPos.z);
        gameObject.transform.position = openPos;
        yield return new WaitForSeconds(time);
        gameObject.transform.position = startPos;
    }

    IEnumerator ColorRed(int time)
    {
        Renderer rend = GetComponent<Renderer>();
        Color current = rend.material.color;
        rend.material.color = Color.red;
        yield return new WaitForSeconds(time);
        rend.material.color = current;
    }
}
