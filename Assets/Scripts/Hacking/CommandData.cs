﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandData : MonoBehaviour
{
    public GameObject hackedObj;
    public string command;
    public int seconds;
}
