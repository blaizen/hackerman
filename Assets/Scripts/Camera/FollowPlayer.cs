﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Vector3 offset;
    public GameObject player;
	
	void Update ()
    {
        transform.position = player.transform.position + offset;
	}
}
