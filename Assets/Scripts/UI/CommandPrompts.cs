﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CommandPrompts : MonoBehaviour
{
    InputField field;

    private void Awake()
    {
        field = GetComponent<InputField>();
    }

    public void Error()
    {
        StartCoroutine(ErrorRoutine());
    }

    IEnumerator ErrorRoutine()
    {
        Image img = GetComponent<Image>();
        img.color = Color.red;
        yield return new WaitForSeconds(3f);
        img.color = Color.black;
    }
}
