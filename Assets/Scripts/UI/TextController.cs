﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    Text docCount;
    public GameObject documentHolder;

    private void Awake()
    {
        docCount = GetComponent<Text>();
        docCount.text = documentHolder.transform.childCount.ToString();
    }

    void DecreaseCount()
    {
        docCount.text = (int.Parse(docCount.text)-1).ToString();
    }
}
