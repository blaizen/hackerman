﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlinkingText : MonoBehaviour
{
    Text placeholderText;
    public float flashTime = 1f;

    private void Awake()
    {
        placeholderText = GetComponent<Text>();
        StartCoroutine(FlashCursor());
    }

    IEnumerator FlashCursor()
    {
        placeholderText.text = "user$:_";
        yield return new WaitForSeconds(flashTime);
        placeholderText.text = "user$:";
        yield return new WaitForSeconds(flashTime);
        StartCoroutine(FlashCursor());
    }
}
