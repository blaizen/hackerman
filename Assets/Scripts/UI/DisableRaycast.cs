﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DisableRaycast : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool inUI = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        inUI = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        inUI = false;
    }
}
