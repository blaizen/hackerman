﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayName : MonoBehaviour {

    public Text displayNameText;
    Ray ray;
    RaycastHit hit;

	void Start ()
    {
        displayNameText.gameObject.SetActive(false);
    }
	
	void Update ()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Input.GetKey(KeyCode.LeftShift))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag("Hackable"))
                    {
                        displayNameText.gameObject.SetActive(true);
                        displayNameText.text = hit.collider.name;
                        displayNameText.transform.position = Input.mousePosition;
                    }
                    else
                    {
                        displayNameText.gameObject.SetActive(false);
                    }
                }
            }
        }
        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            displayNameText.gameObject.SetActive(false);
        }
	}
}
