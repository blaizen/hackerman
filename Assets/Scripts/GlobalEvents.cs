﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalEvents : MonoBehaviour
{
    public static GlobalEvents instance;

    private void Awake()
    {
        instance = new GlobalEvents();
    }

    public void WrongCommand()
    {
        GameObject temp = GameObject.Find("Command Input Field");
        InputField field = temp.GetComponent<InputField>();
        field.GetComponent<CommandPrompts>().Error();
        Debug.Log("This command doesn't exist! (Could it be for a different type of object?)");
    }

    public void DocumentPickup()
    {
        GameObject player = GameObject.Find("Player");
        player.GetComponent<PlayerInventory>().GetDocument();
        GameObject.Find("Number").SendMessage("DecreaseCount");
    }
}
