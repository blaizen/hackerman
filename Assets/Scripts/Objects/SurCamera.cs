﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurCamera : MonoBehaviour
{
    /*
     * This will eventually become a state machine, at the moment it just calls functions!!
     */


    GameObject player;
    public float visionDistance;
    public float visionCone;
    public bool disabled = false;

    float alertTimer = 0;
    public float cooldownTimer;

    public float rotationSpeed = 100.0f;

    Vector3 startPos;
    Quaternion startRot;

    public Light spotLight;
    public Light pointLight;
    Color greenLight;

    public enum States
    {
        Idle,
        Alert,
        Caution
    }

    private States states;

    private void Awake()
    {
        player = GameObject.Find("Player");
        startPos = transform.position;
        startRot = transform.rotation;
        greenLight = spotLight.color;
        Debug.Log(startRot);
    }

    private void Update()
    {
        switch (states)
        {
            case States.Idle:
                Idle();
                break;
            case States.Alert:
                Alert();
                break;
            case States.Caution:
                Caution();
                break;
            default:
                break;
        }
    }

    void Idle()
    {
        if(!disabled)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, startRot, Time.deltaTime * rotationSpeed);
            spotLight.color = greenLight;
            pointLight.color = greenLight;
            if (CanSeePlayer())
            {
                states = States.Alert;
            }
        }
    }

    void Alert()
    {
        transform.LookAt(player.transform);
        spotLight.color = Color.red;
        pointLight.color = Color.red;
        alertTimer = 0;
        if(!CanSeePlayer())
        {
            states = States.Caution;
        }
    }

    void Caution()
    {
        spotLight.color = Color.yellow;
        pointLight.color = Color.yellow;
        alertTimer += Time.deltaTime;
        if(alertTimer > cooldownTimer)
        {
            states = States.Idle;
            alertTimer = 0;
        }
        if(CanSeePlayer())
        {
            states = States.Alert;
        }
    }

    public IEnumerator Disabled(int time)
    {
        disabled = true;
        spotLight.color = Color.blue;
        pointLight.color = Color.blue;
        yield return new WaitForSeconds(time);
        disabled = false;
    }

    bool CanSeePlayer()
    {
        Vector3 forward = transform.forward;
        Vector3 dif = player.transform.position - transform.position;

        float distToPlayer = dif.magnitude;
        Vector3 dirToPlayer = dif.normalized;

        if (distToPlayer > visionDistance)
        {
            return false;
        }

        var angleBetween = Mathf.Abs(Vector3.Angle(forward, dirToPlayer));
        if (angleBetween > visionCone)
        {
            return false;
        }

        RaycastHit hit;
        Physics.Raycast(transform.position, dirToPlayer, out hit);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.name != "Player")
            {
                return false;
            }
        }
        return true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, transform.forward * visionDistance);
        var rot = Quaternion.Euler(0, visionCone, 0) * transform.forward;
        Gizmos.DrawRay(transform.position, rot * visionDistance);
        var negRot = Quaternion.Euler(0, -visionCone, 0) * transform.forward;
        Gizmos.DrawRay(transform.position, negRot * visionDistance);
        var upRot = Quaternion.Euler(visionCone, 0, 0) * transform.forward;
        Gizmos.DrawRay(transform.position, upRot * visionDistance);
        var downRot = Quaternion.Euler(-visionCone, 0, 0) * transform.forward;
        Gizmos.DrawRay(transform.position, downRot * visionDistance);
    }
}
