﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

    public float speed = 200f;

	void Update ()
    {
        transform.Rotate(new Vector3(0, speed, 0) * Time.deltaTime);
	}
}
