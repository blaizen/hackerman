﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Document : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GlobalEvents.instance.DocumentPickup();
        Destroy(gameObject);
    }
}
